# Generates a random dataframe,
# then calculates the correlation between columns
# that are shifted by different numbers of rows
# and generates a plt graph for visualization

import pandas as pd
from random import randint
import matplotlib.pyplot as plt

num_cols = 4

df = pd.DataFrame(index = range(9))
for index in df.index:
	for col in range(num_cols):
		df.loc[index, col] = randint(0, 9)

print(f'df =\n{df}\n')

for shift in range(0, num_cols):
	df1 = df.copy()
	df1[shift] = df[shift].shift(shift)
	print(f'df[{shift}] shifted by {shift} =\n{df1}')
	print(f'corr = {df[0].corr(df1[shift])}\n')
	df1[shift] = df[shift].shift(-1 * shift)
	print(f'df[{shift}] shifted by {-shift} =\n{df1}')
	print(f'corr = {df[0].corr(df1[shift])}\n')

x = df.index
y1 = df[0]
y2 = df[0].shift(1)
y3 = df[0].shift(-1)
ax1 = plt.plot(x, y1, 'g-', label = 'Unshifted')
ax2 = plt.plot(x, y2, 'b-', label = 'Shifted by 1')
ax3 = plt.plot(x, y3, 'y-', label = 'Shifted by -1')

plt.xlabel('Time')
plt.ylabel('Returns')
plt.legend()

plt.suptitle('Unshifted vs shifted')
plt.show()